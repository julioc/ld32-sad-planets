using UnityEngine;

public class PlayerEnergy : MonoBehaviour
{
  public int startingEnergy = 10;
  public int maxEnergy = int.MaxValue;
  public int currentEnergy;

  private void Awake()
  {
    currentEnergy = startingEnergy;
  }

  private void PickupEnergy(EnergyPickup pickup)
  {
    currentEnergy += pickup.energy;

    Destroy(pickup.gameObject);
  }

  public bool SpendEnergy(int amount)
  {
    if(currentEnergy < amount) {
      return false;
    }

    currentEnergy -= amount;
    return true;
  }
}