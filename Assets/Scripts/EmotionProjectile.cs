using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class EmotionProjectile : MonoBehaviour
{
  public int delta = 1;
  public GameObject explosion;

  private const float radius = 0.5f;

  private void OnTriggerEnter2D(Collider2D other)
  {
    if(other.tag == "Enemy") {
      if(HitEnemy(other.gameObject)) {
        Explode();
      }
    }
    if(other.gameObject.layer == Layers.Terrain) {
      var position2D = new Vector2(transform.position.x, transform.position.z);
      Collider2D[] enemies = Physics2D.OverlapCircleAll(position2D, radius, Layers.Enemies);
      foreach(Collider2D enemy in enemies) {
        HitEnemy(enemy.gameObject);
      }

      Explode();
    }
  }

  private bool HitEnemy(GameObject enemy)
  {
    var emotional = enemy.GetComponent<Emotional>();
    if(emotional != null) {
      if(emotional.ChangeEmotion(delta)) {
        return true;
      }
    }
    return false;
  }

  private void Explode()
  {
    Instantiate(explosion, transform.position, Quaternion.identity);
    Destroy(gameObject);
  }
}