using UnityEngine;

public class ModuleInfo : MonoBehaviour
{
  public int width;
  public Transform start;
  public Transform end;
  public Transform bottom;
  public Transform spawn;
  public GameObject baseTile;

  private float tileSize = 1.28f;
  private int depth = 10;

  private void Awake()
  {
    float halfTile = tileSize*0.5f;
    var startinPos = new Vector3(bottom.position.x + halfTile, bottom.position.y - halfTile, 0f);
    for(int x = 0; x < width; ++x) {
      for(int y = 0; y < depth; ++y) {
        var coords = new Vector3(x, -y, 0);
        var pos = startinPos + (coords * tileSize);
        var tile = Instantiate(baseTile, pos, Quaternion.identity) as GameObject;
        tile.transform.parent = transform;
      }
    }
  }
}