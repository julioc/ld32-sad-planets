using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
  private MyController gameController;

  private void Awake()
  {
    gameController = GameObject.FindWithTag("GameController").GetComponent<MyController>();
  }

  public void Hurt(int amount)
  {
    Debug.Log("Hurt: " + amount);
    // Debug.Log(gameController.game)
    gameController.GameOver();
  }
}