using UnityEngine;

public class Emotional : MonoBehaviour
{
  public int starting;
  public int minimun = 0;
  public int maximum = 10;

  public int current;

  private void Awake()
  {
    current = starting;
  }

  public int GetEmotion()
  {
    return current;
  }

  public bool SetEmotion(int emotion)
  {
    emotion = Mathf.Clamp(emotion, minimun, maximum);

    bool changed = (current != emotion);
    if(changed) {
      current = emotion;
      gameObject.SendMessageUpwards("OnEmotionChange", current);
    }

    return changed;
  }

  public bool ChangeEmotion(int delta)
  {
    return SetEmotion(current + delta);
  }
}