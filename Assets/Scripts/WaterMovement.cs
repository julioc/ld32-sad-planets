using UnityEngine;

public class WaterMovement : MonoBehaviour
{
  public Vector3 speed;

  private float count;

  private Vector3 start;

  private void Awake()
  {
    start = transform.position;
  }

  private void Update()
  {
    count += Time.deltaTime;
    transform.position = start + (speed * Mathf.Cos(count * Mathf.PI));
  }
}