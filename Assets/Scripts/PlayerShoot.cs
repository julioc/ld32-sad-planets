using UnityEngine;

[RequireComponent(typeof(PlayerEnergy))]
public class PlayerShoot : MonoBehaviour
{
  public int energyCost = 1;
  public Transform origin;
  public GameObject projectilePrefab;
  public float projectileSpeed;

  private PlayerEnergy energy;

  private void Awake()
  {
    energy = GetComponent<PlayerEnergy>();
  }

  private void Update()
  {
    if(Input.GetButtonDown("Fire1")) {
      Vector3 mouseDirection = Input.mousePosition;
      mouseDirection.z = 0;
      mouseDirection = Camera.main.ScreenToWorldPoint(mouseDirection);
      mouseDirection = mouseDirection - origin.position;
      Shoot(mouseDirection);
    }
  }

  private void Shoot(Vector3 direction)
  {
    if(energy.SpendEnergy(energyCost)) {
      var projectileInstance = Instantiate(projectilePrefab, origin.position, Quaternion.Euler(new Vector3(0,0,0))) as GameObject;
      if(projectileInstance != null) {
        var speed = new Vector2(direction.x, direction.y);
        speed = speed.normalized;
        speed *= projectileSpeed;

        projectileInstance.GetComponent<Rigidbody2D>().velocity = speed;
      }
    }
    // TODO: indicator that has no energy
  }
}