using UnityEngine;

public class Finish : MonoBehaviour
{
  private void OnTriggerEnter2D(Collider2D other)
  {
    if(other.tag == "Player") {
      GameObject.FindWithTag("GameController").GetComponent<MyController>().FinishedLevel();
    }
  }
}