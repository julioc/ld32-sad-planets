using UnityEngine;

public class ModuleLoader : MonoBehaviour
{
  public GameObject[] spawnModules;
  public GameObject[] modules;
  public GameObject[] finishModules;

  public void Generate(int minWidth, out Vector3 spawnPosition, out int count)
  {
    int width = 0;
    Vector3 lastPos = transform.position;

    GameObject spawnPrefab = spawnModules[Random.Range(0, spawnModules.Length)];
    ModuleInfo spawnInfo = GenerateModule(spawnPrefab, ref lastPos);
    spawnPosition = spawnInfo.spawn.position;

    count = 0;
    int lastIndex = -1;
    while(width < minWidth) {
      int index;
      do {
        index = Random.Range(0, modules.Length);
      } while(lastIndex == index);
      GameObject prefab = modules[index];

      ModuleInfo info = GenerateModule(prefab, ref lastPos);
      width += info.width;

      lastIndex = index;
      count++;
    }

    GameObject finishPrefab = finishModules[Random.Range(0, finishModules.Length)];
    GenerateModule(finishPrefab, ref lastPos);
  }

  private ModuleInfo GenerateModule(GameObject prefab, ref Vector3 pos)
  {
    var module = Instantiate(prefab, pos, Quaternion.identity) as GameObject;
    if(module != null) {
      ModuleInfo info = module.GetComponent<ModuleInfo>();

      module.transform.parent = transform;

      module.transform.position -= info.start.localPosition;
      pos = info.end.position;

      return info;
    }
    return null;
  }
}