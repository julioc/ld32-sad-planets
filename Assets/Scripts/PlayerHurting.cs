using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class PlayerHurting : MonoBehaviour
{
  public int amount = -1;

  private void OnTriggerEnter2D(Collider2D other)
  {
    if(other.gameObject.tag == "Player") {
      PlayerHealth health = other.gameObject.GetComponent<PlayerHealth>();
      health.Hurt(amount);
    }
  }
}