using UnityEngine;
using UnityEngine.UI;

public class MyController : MonoBehaviour
{
  public GameObject playerPrefab;
  public GameObject gameOverImage;
  public Text scoreText;

  public Text hudScoreText;
  public Text hudEnergyText;

  public int planets;
  public int things;

  private GameObject player;
  private PlayerEnergy playerEnergy;
  private ModuleLoader loader;

  private void Awake()
  {
    int energy = 0;
    if(PlayerPrefs.HasKey("energy")) {
      energy = PlayerPrefs.GetInt("energy");
      if(energy > 0) {
        things = PlayerPrefs.GetInt("things");
        planets = PlayerPrefs.GetInt("planets");
      }
    }

    int width = 30 + planets * 5;
    energy += width / 5;

    gameOverImage.SetActive(false);
    loader = GetComponent<ModuleLoader>();

    Vector3 spawnPosition;
    int platforms;
    loader.Generate(width, out spawnPosition, out platforms);

    player = Instantiate(playerPrefab, spawnPosition, Quaternion.identity) as GameObject;
    Camera.main.gameObject.transform.position = new Vector3(spawnPosition.x, spawnPosition.y, -10f);
    Camera.main.gameObject.GetComponent<UnityStandardAssets._2D.Camera2DFollow>().target = player.transform;
    playerEnergy = player.GetComponent<PlayerEnergy>();
    playerEnergy.currentEnergy = energy;
  }

  private void Update()
  {
    hudScoreText.text = "Score: " + things;
    hudEnergyText.text = "Energy: " + playerEnergy.currentEnergy;
  }

  public void GameOver()
  {
    gameOverImage.SetActive(true);
    scoreText.text = "saved " + planets + " planets and" + "\n";
    scoreText.text += "made " + things + " happier things";

    player.GetComponent<PlayerControl>().enabled = false;
    player.GetComponent<PlayerShoot>().enabled = false;
    player.GetComponent<Rigidbody2D>().isKinematic = true;
    player.GetComponent<Animator>().SetTrigger("Death");

    Invoke("RestartGame", 5.0f);
  }

  public void RestartGame()
  {
    PlayerPrefs.DeleteKey("things");
    PlayerPrefs.DeleteKey("planets");
    PlayerPrefs.DeleteKey("energy");
    Application.LoadLevel(Application.loadedLevel);
  }

  public void FinishedLevel()
  {
    PlayerPrefs.SetInt("things", things);
    PlayerPrefs.SetInt("planets", planets+1);
    PlayerPrefs.SetInt("energy", playerEnergy.currentEnergy);
    Application.LoadLevel(Application.loadedLevel);
  }
}