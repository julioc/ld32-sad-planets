using UnityEngine;

public class PingPongMover : MonoBehaviour
{
  public Vector3 range;
  public float timeScale = 1f;
  public bool flipHorizontal = true;

  private float count;
  private bool goingBack;

  private void Update()
  {
    float delta = (Time.deltaTime * timeScale) * (goingBack ? -1f : 1f);
    count = Mathf.Clamp(count + delta, -1f, 1f);

    transform.position += delta * range;

    if(count >= 1f || count <= -1f) {
      goingBack = !goingBack;
      if(flipHorizontal) {
        transform.localScale = new Vector3(transform.localScale.x * -1f, transform.localScale.y, transform.localScale.z);
      }
    }
  }


}