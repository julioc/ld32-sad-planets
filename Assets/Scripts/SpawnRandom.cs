using UnityEngine;

public class SpawnRandom : MonoBehaviour
{
  public GameObject[] prefabs;

  private void Awake()
  {
    GameObject prefab = prefabs[Random.Range(0, prefabs.Length)];
    var obj = Instantiate(prefab, transform.position, Quaternion.identity) as GameObject;
    if(obj != null) {
      obj.transform.parent = transform;
    }
  }
}