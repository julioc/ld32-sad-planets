using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class EnergyPickup : MonoBehaviour
{
  public int energy = 1;

  private void OnTriggerEnter2D(Collider2D other)
  {
    if(other.tag == "Player") {
      other.gameObject.SendMessageUpwards("PickupEnergy", this);
    }
  }
}