using UnityEngine;

[RequireComponent(typeof(Emotional))]
public class EmotionalObjetSwaper : MonoBehaviour
{
  public int threshold = 1;
  public GameObject[] lower;
  public GameObject[] higher;

  private void OnEmotionChange(int emotion)
  {
    SetActiveAll(lower, (emotion < threshold));
    SetActiveAll(higher, (emotion >= threshold));
  }

  private void SetActiveAll(GameObject[] objects, bool active)
  {
    if(objects != null) {
      foreach(GameObject obj in objects) {
        obj.SetActive(active);
      }
    }
  }
}