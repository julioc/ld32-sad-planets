using UnityEngine;

[RequireComponent(typeof(Emotional))]
public class EmotionalEnemy : MonoBehaviour
{
  private void OnEmotionChange(int emotion)
  {
    MyController controller = GameObject.FindWithTag("GameController").GetComponent<MyController>();
    controller.things += 1;
  }
}